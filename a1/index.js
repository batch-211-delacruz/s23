//console.log('Hello World!');

let trainer = {
	name: "Trainer Red",
	age: "18",
	pokemon: ["Charizard","Venusaur","Lapras","Arcanine","Pikachu","Blastoise"],
	friends: {
		hoenn: ["Brendan","Flannery","Winona"],
		kanto: ["Blaine","Sabrina","Lorelei"]
	},
	talk: function(){
		console.log("Pikachu! I choose you!")
	}
}
console.log(trainer);
console.log("Result of dot notation: ");
console.log(trainer.name);
console.log("Result of square bracket notation: ");
console.log(trainer["pokemon"]);
console.log("Result of talk method: ");
trainer.talk();

function Pokemon(name,level){

			//properties
			this.name = name;
			this.level = level;
			this.health = level * 2;
			this.attack = level * 1.5;
			this.move = level * 3;
			this.flamethrower = function(target){
				console.log(this.name + " used flamethrower" + target.name);
				target.health -= this.move

				console.log(target.name + " health is now reduced to " + target.health);

				if(target.health<=0){
					target.faint()
				}
			}
			this.tackle = function(target){
				console.log(this.name + " tackled " + target.name);

				target.health -= this.attack

				console.log(target.name + " health is now reduced to " + target.health);

				if(target.health<=0){
					target.faint()
				}

			}
			this.faint = function(){
				console.log(this.name + " fainted.")
			}

		}
	let charizard = new Pokemon ("Charizard", 60);
	console.log(charizard);
	let snorlax = new Pokemon ("Snorlax", 50);
	console.log(snorlax);